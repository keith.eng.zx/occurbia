/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    screens: {
      mobile: { max: '767px' },
      tablet: { min: '768px' },
      smallDesktop: { min: '1024px' },
      bigDesktop: { min: '1280px' },
    },
    extend: {
      colors: {
        bg: {
          light: '#fff',
          DEFAULT: '#050505',
          dark: '#050505',
        },
        text: {
          white: '#fefdf8',
          DEFAULT: '#242726',
          black: '#242726',
          grey: '#54565f',
        },
        space: {
          grey: {
            DEFAULT: 'rgb(37, 38, 45)',
            opaque: 'rgb(37, 38, 45, 0.95)',
          },
          orange: {
            DEFAULT: 'rgb(242,120,54)',
            opaque: 'rgb(242,120,54,0.8)',
          },
          'blood-orange': '#d34d3b',
          yellow: '#f8da0c',
          red: '#e33041',
          green: '#03b088',
          'lime-green': '#32CD32',
        },
      },
    },
  },
  plugins: [],
};