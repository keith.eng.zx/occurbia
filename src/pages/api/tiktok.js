const { WebcastPushConnection } = require('tiktok-live-connector');

export default async function handler(req, res) {
    const tiktokUsername = req.query.user;
    if (!tiktokUsername) {
        res.status(400).json({ message: 'Enter user in params' })
        return
    }

    try {
        const tiktokLiveConnection = new WebcastPushConnection(tiktokUsername);

        // Connect to the chat (await can be used as well)
        await tiktokLiveConnection.connect().then(state => {
            console.info(`Connected to roomId ${state.roomId}`);
        }).catch(err => {
            res.status(400).json({ message: `Failed to connect: ${err}` })
            throw new Error(err);
        })

        // Define the events that you want to handle
        // In this case we listen to chat messages (comments)
        tiktokLiveConnection.on('chat', data => {
            console.log(`${data.uniqueId} (userId:${data.userId}) writes: ${data.comment}`);
        })

        // And here we receive gifts sent to the streamer
        tiktokLiveConnection.on('gift', data => {
            console.log(`${data.uniqueId} (userId:${data.userId}) sends ${data.giftId}`);
        })

        res.status(200).json({ name: tiktokUsername })
    } catch (e) {
        res.status(400).json({ message: e })
        return
    }
}
