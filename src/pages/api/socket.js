import { Server } from 'Socket.IO'
const { WebcastPushConnection } = require('tiktok-live-connector');

const SocketHandler = (req, res) => {
    let tiktokLiveConnection;

    if (res.socket.server.io) {
        console.log('Socket is already running')
    } else {
        console.log('Socket is initializing')
        const io = new Server(res.socket.server)
        res.socket.server.io = io;

        io.on('connection', socket => {
            console.log('Socket is connected')

            socket.on('connect-room', (roomId) => {
                tiktokLiveConnection = new WebcastPushConnection(roomId);
                tiktokLiveConnection.connect().then(state => {
                    console.info(`Connected to roomId ${state.roomId}`);
                    socket.broadcast.emit('connected-room')
                }).catch(err => {
                    console.error('Failed to connect', err);
                    socket.broadcast.emit('failed-room')
                })

                tiktokLiveConnection.on('chat', data => {
                    console.log(`${data.uniqueId} writes: ${data.comment}`);
                    socket.broadcast.emit('message', {
                        username: data.uniqueId || '',
                        message: data.comment || ''
                    })
                })
            });
        })
    }

    res.end()
}

export default SocketHandler