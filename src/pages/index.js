import Head from 'next/head'
import { Inter } from 'next/font/google'
import { useEffect, useState } from 'react'
import io from 'Socket.IO-client';

import { MemoizedPlayer } from '@/components/molecules/player';

let socket;
const inter = Inter({ subsets: ['latin'] })

const Home = () => {
  const [playerList, setPlayerList] = useState([]);
  const [roomId, setRoomId] = useState('');
  const [status, setStatus] = useState('');

  useEffect(() => {
    socketInitializer(playerList)
  }, [playerList]);

  const socketInitializer = async (playerList) => {
    await fetch('/api/socket')
    socket = io()

    socket.on('connect', () => {
      setStatus('socket connected')
    })

    socket.on('disconnect', () => {
      setStatus('socket disconnected')
    })

    socket.on('connected-room', () => {
      setStatus('room connected')
    })

    socket.on('failed-room', () => {
      setStatus('room failed to connect')
    })

    socket.on('message', ({ username, message }) => {
      setStatus('Received new message')

      setPlayerList(currentList => {
        if (currentList.some(player => player?.username === username)) {
          return [...currentList]
        } else {
          return [...currentList, {
            username: username,
            money: 0
          }]
        }
      })
    })
  }

  return (
    <>
      <Head>
        <title>Occurbia</title>
        <meta name="description" content="A TikTok-Live based interactive game" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={inter.className}>
        <div
          id="controls"
        >
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              gap: '5px'
            }}
          >
            <label htmlFor="roomid">Enter room id:</label>
            <input
              type="text"
              id="roomid"
              name="roomid"
              style={{
                padding: '2px 10px 2px'
              }}
              value={roomId}
              onChange={(e) => setRoomId(e.target.value)}
            />
            <button
              style={{
                border: '1px solid white',
                paddingLeft: '5px',
                paddingRight: '5px'
              }}
              onClick={() => {
                socket.emit('connect-room', roomId)
              }}
            >
              Launch!
            </button>
          </div>
          <span
            style={{
              display: 'flex',
              justifyContent: 'center',
              color: 'green'
            }}
          >
            Status: {status}
          </span>
        </div>
        <div
          id="kingdom"
          style={{
            backgroundImage: 'url("/assets/backgrounds/cave_background.jpg")',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            backgroundPosition: 'bottom'
          }}
        >
          {playerList ? (
            playerList.map((player) => {
              return <MemoizedPlayer username={player.username} money={player.money} />
            })
          ) : <></>}
        </div>
      </main>
    </>
  )
}

export default Home;