import '@/styles/globals.css'
import styles from '@/styles/index.scss'

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}
