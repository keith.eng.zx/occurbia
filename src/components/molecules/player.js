import { memo } from 'react';
import Image from 'next/image'

const Player = ({ username, money }) => {
    const mainClassname = 'player';

    return (
        <div id='player' className={`${mainClassname}`}
            style={{
                animationDelay: `${Math.random() * 25}s`,
                animationDuration: `${Math.random() * 30 + 25}s`
            }}
        >
            <span className={`${mainClassname}__money`}>${money || '0'}</span>
            <span className={`${mainClassname}__name`}>{username || 'null'}</span>
            <div className={`${mainClassname}__image`}>
                <div className={`${mainClassname}__image__body`}>
                    <Image src='/assets/base/stickman.png' alt="me" width="90" height="90" />
                </div>
                <div className={`${mainClassname}__image__clothes`}>
                    <Image src='/assets/clothes/caveman_robes.png' alt="clothes" width="76" height="60" />
                </div>
                <div className={`${mainClassname}__image__weapon`}>
                    <Image src='/assets/weapons/club.png' alt="weapon" width="22" height="112" />
                </div>
            </div>
        </div >
    )
}

export const MemoizedPlayer = memo(Player);